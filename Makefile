BUILD=build

all:	compile run clean

compile: 
	cd $(BUILD) && $(MAKE) compile

run:
	cd $(BUILD) && $(MAKE) run

clean:
	cd $(BUILD) && $(MAKE) clean
