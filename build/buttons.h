#ifndef _BUTTONS_H_
#define _BUTTONS_H_

#include <iostream>
#include <thread>
#include "wiringPi.h"

class Buttons{
private:
    int gpio_nr;
public:
    Buttons() = default;
    Buttons(int gpio, std::string desc);
    int get_gpio();
    std::string description;
    bool short_time = false;
    bool long_time = false;
};

class Keyboard{
private:
public:
    Buttons b_ok;
    Buttons b_cancel;
    Buttons b_left;
    Buttons b_right;
    Keyboard(int gpio_ok, std::string desc_ok, int gpio_cancel, std::string desc_cancel, int gpio_left, std::string desc_left, int gpio_right, std::string desc_right);
    short check_button_status(Buttons& button);
    void if_buttons_have_been_pressed(bool* thread_kill);
    bool check_if_all_buttons_are_short();
    void reset_all_buttons();
};

#endif // _BUTTONS_H_
