#ifndef _THREADS_H_
#define _THREADS_H_

#include <iostream>
#include <thread>
#include <array>
#include <ads1115.h>
#include <wiringPi.h>

#include "batteries.h"
#include "lcd_display.h"

class ADS1115{
private:
    std::array<int,4> pin;
    const int address;
    int gain;
    float resolution;
    float conversion15bit = 32768.0f;
public:
    ADS1115(int pinbase, int _address);
    void set_gain(int _gain);
    int get_gain();
    int get_value(int channel);
    float get_volts(int value);
    bool thread_read_write_stop = false;
    std::thread thread_read_write;
};

void thread_read_write_voltage(Batt& Battery, ADS1115& Voltmeter);



#endif // _THREADS_H_
