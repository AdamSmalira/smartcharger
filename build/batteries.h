#ifndef _BATTERIES_H_
#define _BATTERIES_H_

#include <iostream>
#include <vector>
#include <exception>
#include <wiringPi.h>
#include <thread>

class Signals;
class Cell;

/* Batt class operates on container of cells */
class Batt
{
private:
    std::vector<Cell> All;
public:
    bool stop_charging_thread = false;   
    Signals& Charger;
    Batt(Signals& obj);
    ~Batt();
    Cell& get_cell(size_t nr);
    void add_cell(int _gpio);
    void change_to_input();
    void change_to_output();
    void stop_chrg_all();
    void start_chrg(Cell& cell_start);
    void channel_test();
};

/* Cell class operates on single cell */
class Cell
{
    /* only this method can call private Cell constructor */
    friend void Batt::add_cell(int);
private:
    const int gpio;
    const int id;
    bool cell_busy = false;
    volatile float voltage;
    Cell(int _gpio, int _id);
public:
    /* getters */
    int  get_gpio();
    int  get_id();
    bool get_busy();
    const int get_gpio() const;
    const int get_id() const;
    const bool get_busy() const;
    float get_voltage();
    /* setters */
    void set_busy(bool value);
    void set_voltage(float volts);
};

/* Signal class operates on charger signals */
class Signals
{
private:
    const int gpio_waiting;
    const int gpio_chrging;
    int countodown_seconds;
    /* Limits to modyfie for the user */
    int charge_time_limit = 15;
    float charge_voltage_limit = 4.1f;
public:
    Signals(int gpio_wait, int gpio_chrg);
    Signals(const Signals& copy);
    const float max_charge_value = 4.2f;
    const float min_charge_value = 3.0f;
    const int max_charge_time = 240;
    const int min_charge_time = 0;
    std::thread thread_countdown;
    bool thread_countdown_stop = false;
    
    const bool get_waiting() const;
    const bool get_chrging() const;
    int get_time_limit();
    float get_voltage_limit();
    int get_countdown_seconds(void);
    void set_time_limit(int minutes);
    void set_voltage_limit(float volts);
    void set_countdown_seconds(int seconds);

};

void thread_start_countdown(Batt& Battery);

#endif //_BATTERIES_H_
