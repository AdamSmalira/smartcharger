#include "batteries.h"

/* Class Cell */
Cell::Cell(int _gpio, int _id): gpio(_gpio), id(_id) { std::cout << "Initialisating at gpio: " << gpio << " Cell nr: " << id << std::endl;}
int Cell::get_gpio() {return gpio;}
int Cell::get_id()   {return id;}
bool Cell::get_busy() {return cell_busy;}
const int Cell::get_gpio() const {return gpio;}
const int Cell::get_id() const {return id;}
const bool Cell::get_busy() const {return cell_busy;}
float Cell::get_voltage() {return voltage;}
void Cell::set_busy(bool value) {cell_busy = value;}
void Cell::set_voltage(float volts) {voltage = volts;}

/* Class Batt */
Batt::Batt(Signals& obj): Charger(obj) {}
Batt::~Batt() { this->change_to_input();}
Cell& Batt::get_cell(size_t nr) {return All.at(nr);}

void Batt::add_cell(int _gpio){
    Cell obj(_gpio, All.size());
    All.push_back(std::move(obj));
}

void Batt::change_to_input(){
    for(const auto& cell: All)
    {
        digitalWrite(cell.get_gpio(), LOW);
        pinMode(cell.get_gpio(), INPUT); 
    }
}

void Batt::change_to_output(){
    for(const auto& cell: All)
    {
        pinMode(cell.get_gpio(), OUTPUT); 
        digitalWrite(cell.get_gpio(), LOW);
    }
}
void Batt::stop_chrg_all()
{
    for(auto& cell: All)
    {
        digitalWrite(cell.get_gpio() , LOW);
        if(true == cell.get_busy())
            std::cout << "Stoping cell nr: " << cell.get_id() << std::endl;
        cell.set_busy(false);
    }
    delay(50);
}

void Batt::start_chrg(Cell& cell_start)
{
/* Safety conditions */    
    stop_chrg_all();
    /* busy || not idle charger condition */
    if(true == Charger.get_chrging() || false == Charger.get_waiting())
    {
        std::cout << "charger is not stoped" << std::endl;
        throw std::exception();
    }
    /* GPIO HIGH state value condition == opened relays */
    for(const auto& cell: All)
    {
        if(true == digitalRead(cell.get_gpio()))
        {
            std::cout << "gpio OUT HIGH - fail" << std::endl;
            throw std::exception();
        }
    }

/* Turning charging ON */
    digitalWrite(cell_start.get_gpio(), HIGH);
    cell_start.set_busy(true);

/* Turning ON countdown mechanism if set */
    if(Charger.get_time_limit() != 0) // 0 == pernament charge
    {
        Charger.thread_countdown_stop = false;
        Charger.thread_countdown = std::thread(thread_start_countdown, std::ref(*this));
    }
    //delay(5000);

/*
*/

}

void Batt::channel_test()
{
    int buff_time = Charger.get_time_limit();
    float buff_volt = Charger.get_voltage_limit();
    Charger.set_time_limit(0);
    Charger.set_voltage_limit(Charger.max_charge_value);

    for(auto& cell: All)
    {
        start_chrg(cell);
        delay(200);
    }
    stop_chrg_all();

    Charger.set_time_limit(buff_time);
    Charger.set_voltage_limit(buff_volt);
    //delay(100);
}


/* Class Signals */
Signals::Signals(int gpio_wait, int gpio_chrg): gpio_waiting(gpio_wait), gpio_chrging(gpio_chrg)
{
    std::cout << "Initialisating at gpio: " << gpio_waiting << " signal for waiting" << std::endl;
    std::cout << "Initialisating at gpio: " << gpio_chrging << " signal for charging" << std::endl;
    pinMode(gpio_chrging, INPUT);
    pinMode(gpio_waiting, INPUT);
}
Signals::Signals(const Signals& copy): gpio_waiting(copy.gpio_waiting), gpio_chrging(copy.gpio_chrging) {}


const bool Signals::get_waiting() const {return digitalRead(gpio_waiting);}
const bool Signals::get_chrging() const {return digitalRead(gpio_chrging);}

int Signals::get_time_limit()           {return charge_time_limit;}
float Signals::get_voltage_limit()      {return charge_voltage_limit;}
int Signals::get_countdown_seconds()    {return countodown_seconds;}
void Signals::set_time_limit(int minutes)       {charge_time_limit = minutes;}
void Signals::set_voltage_limit(float volts)    {charge_voltage_limit = volts;}
void Signals::set_countdown_seconds(int seconds)    {countodown_seconds = seconds;}


/* thread */
void thread_start_countdown(Batt& Battery)
{
    int time = Battery.Charger.get_time_limit() * 60;
    Battery.Charger.set_countdown_seconds(time);
    while(time)
    {
        std::cout << "ODLICZNAIE: " << time << std::endl;
        delay(1000);
        --time;
        Battery.Charger.set_countdown_seconds(time);

        if(Battery.Charger.thread_countdown_stop)
        {
            return;
        }
    }

    Battery.stop_chrg_all();
    Battery.Charger.thread_countdown_stop = true;
    }
