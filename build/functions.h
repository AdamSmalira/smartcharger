#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include <iostream>
#include <cmath>
#include "wiringPi.h"

#include "buttons.h"
#include "batteries.h"
#include "lcd_display.h"
#include "threads.h"

void initialize_platform(int& fd);
void manual_charging(Keyboard& Keys, Lcd& Display, Batt& Battery);
void common_charge_setup(Keyboard& Keys, Lcd& Display, Batt& Battery, int cell_NR);
void common_charge_setup_voltslimit(Keyboard& Keys, Lcd& Display, Batt& Battery);
void common_charge_setup_timelimit(Keyboard& Keys, Lcd& Display, Batt& Battery);

bool float_compare(float a, float b);


#endif //FUNCTIONS_H_
