#include "functions.h"


void initialize_platform(int& fd)
{
    std::cout << "Initializating platform" << std::endl;
    fd = wiringPiSetupGpio();
    if ( fd != 0)
    {
        //throw GPIO_init{*ptr_pig};
    }

    std::cout << "Platform initializated" << std::endl;
}
void manual_charging(Keyboard& Keys, Lcd& Display, Batt& Battery)
{
    std::cout << "./menu/manual/" << std::endl;
    volatile short possition_manual = 0;
    const short possition_manual_min = 0;
    const short possition_manual_max = 3;

    const short possition_bat1 = 0;
    const short possition_bat2 = 1;
    const short possition_bat3 = 2;
    const short possition_bat4 = 3;
    while(true)
    {
        std::cout << "Reset petli " << std::endl;
        if(possition_manual == possition_bat1)
            Display.Manual_chrg->bat(Battery, 0);
        if(possition_manual == possition_bat2)
            Display.Manual_chrg->bat(Battery, 1);
        if(possition_manual == possition_bat3)
            Display.Manual_chrg->bat(Battery, 2);
        if(possition_manual == possition_bat4)
            Display.Manual_chrg->bat(Battery, 3);

        Keys.if_buttons_have_been_pressed(nullptr);

        /* Defining movement right and left buttons in menu */
        if (Keys.b_right.short_time && (possition_manual < possition_manual_max))
        {
            Keys.reset_all_buttons();
            std::cout << "./menu/manual/  button right" << std::endl;
            possition_manual++;
        }
        if (Keys.b_left.short_time && (possition_manual > possition_manual_min))
        {
            Keys.reset_all_buttons();
            std::cout << "./menu/manual/  button left" << std::endl;
            possition_manual--;
        }

        /* Defining actions for pressing OK for each possition */
        if (Keys.b_ok.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << "./menu/manual/ chosen bat1" << std::endl;
            common_charge_setup(Keys, Display, Battery, possition_manual);
        
        }
  
        /* Defining cancel*/
        if (Keys.b_cancel.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << "./menu/manual/ cancel" << std::endl;
            break;
        }
        Keys.reset_all_buttons();
        delay(100);
    }

}

/* Functions used by both modes - automatic and manual */
void common_charge_setup(Keyboard &Keys, Lcd &Display, Batt &Battery, int cell_NR)
{
    volatile short possition_common = 1;
    const short possition_common_min = 1;
    const short possition_common_max = 2;

    const short possition_volts = 1;
    const short possition_time = 2;
    std::cout << "Bateria status: " << Battery.get_cell(cell_NR).get_busy() << std::endl;
    while (true)
    {
        if (possition_common == 1)
            Display.common_charge_volt(Battery.Charger.get_voltage_limit(), Battery.Charger.max_charge_value);
        if (possition_common == 2)
            Display.common_charge_time(Battery.Charger.get_time_limit(), Battery.Charger.min_charge_time);

        Keys.if_buttons_have_been_pressed(nullptr);

        /* Defining movement right and left buttons in menu */
        if (Keys.b_right.short_time && (possition_common < possition_common_max))
        {
            Keys.reset_all_buttons();
            std::cout << " button right" << std::endl;
            possition_common++;
        }
        if (Keys.b_left.short_time && (possition_common > possition_common_min))
        {
            Keys.reset_all_buttons();
            std::cout << " button left" << std::endl;
            possition_common--;
        }

        /* Defining actions for pressing OK for each possition */
        if (Keys.b_ok.short_time && (possition_common == possition_volts))
        {
            Keys.reset_all_buttons();
            std::cout << " seting voltage limit" << std::endl;
            common_charge_setup_voltslimit(Keys, Display, Battery);
        }
        if(Keys.b_ok.short_time && (possition_common == possition_time))
        {
            Keys.reset_all_buttons();
            std::cout << " setting time limit" << std::endl;
            common_charge_setup_timelimit(Keys, Display, Battery);
            //Display.main_not_yet();
            //delay(2000);
        }
        //
        if(Keys.b_ok.long_time && (Battery.get_cell(cell_NR).get_busy() == 0))
        {
            Keys.reset_all_buttons();
            Battery.stop_charging_thread = false;
            Display.thread_lcd = std::thread(thread_display_charging, std::ref(Battery), std::ref(Display), cell_NR);
            std::cout << " charging cell nr: " << Battery.get_cell(0).get_id() << std::endl;
            delay(500);
            Battery.start_chrg(Battery.get_cell(cell_NR));
            while(true)
            {
                Keys.if_buttons_have_been_pressed(&Battery.Charger.thread_countdown_stop);
                if (Keys.check_if_all_buttons_are_short() || Battery.Charger.thread_countdown_stop)
                {
                    Keys.reset_all_buttons();
                    break;
                }
            }
            Battery.stop_charging_thread = true;
            Battery.Charger.thread_countdown_stop = true;
            Battery.stop_chrg_all();
            Display.thread_lcd.join();
            Battery.Charger.thread_countdown.join();
            std::cout << "Finishing charging" << std::endl;
            delay(1000); //to get propper reading after charging
            break;
        }

        /* Defining cancel*/
        if (Keys.b_cancel.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << " cancel" << std::endl;
            break;
        }
        Keys.reset_all_buttons();
        delay(100);
    }
}

void common_charge_setup_voltslimit(Keyboard& Keys, Lcd& Display, Batt& Battery)
{
    float value = Battery.Charger.get_voltage_limit();
    lcdCursorBlink (Display.get_fd(), true);
    
    while (true)
    {
        
        lcdPosition (Display.get_fd(), 15, 1);
        Keys.if_buttons_have_been_pressed(nullptr);
        /* Defining movement right and left buttons in menu for setting voltage limit */
        if (Keys.b_right.short_time)
        {
            Keys.reset_all_buttons();
            if(float_compare(value, Battery.Charger.max_charge_value)){ //Guard not to overset the max value;
                Display.common_charge_volt(value, Battery.Charger.max_charge_value);
                continue;}
            std::cout << " incrementing voltage" << std::endl;
            value += 0.10;
            Display.common_charge_volt(value, Battery.Charger.max_charge_value);
        }
        if (Keys.b_left.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << "BEFORE INSIDE: " << value << std::endl;
            if(float_compare(value, Battery.Charger.min_charge_value)){ //Guard not to overset the max value;
                std::cout << "INSIDE" << std::endl;
                continue;}
            std::cout << " decrementing voltage" << std::endl;
            value -= 0.10;
            Display.common_charge_volt(value, 0);
        }

        /* Defining Ok action as saving the result */
        if (Keys.b_ok.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << " saving new limit" << std::endl;
            Battery.Charger.set_voltage_limit(value);
            lcdCursorBlink (Display.get_fd(), false);
            break;
        }

        /* Defining Cancel action as aborting changes */
        if (Keys.b_cancel.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << " changes not saved" << std::endl;
            lcdCursorBlink (Display.get_fd(), false);
            break;
        }
    }


}

void common_charge_setup_timelimit(Keyboard& Keys, Lcd& Display, Batt& Battery)
{
    int time_value = Battery.Charger.get_time_limit();
    lcdCursorBlink (Display.get_fd(), true);
    
    while (true)
    {
        lcdPosition (Display.get_fd(), 15, 1);
        Keys.if_buttons_have_been_pressed(nullptr);

        /* Defining movement right and left buttons in menu for setting voltage limit */
        if (Keys.b_right.short_time)
        {
            Keys.reset_all_buttons();
            if(time_value == Battery.Charger.max_charge_time){ //Guard not to overset the max value;
                Display.common_charge_time(time_value, Battery.Charger.min_charge_time);
                continue;}
            std::cout << " incrementing +15min" << std::endl;
            time_value += 5;
            Display.common_charge_time(time_value, Battery.Charger.min_charge_time);
        }
        if (Keys.b_left.short_time)
        {
            Keys.reset_all_buttons();
            if(time_value == Battery.Charger.min_charge_time){ //Guard not to overset the min value;
                Display.common_charge_time(time_value, Battery.Charger.min_charge_time);
                continue;}
            std::cout << " decrementing +15min" << std::endl;
            time_value -= 5;
            Display.common_charge_time(time_value, Battery.Charger.min_charge_time);

        }

        /* Defining Ok action as saving the result */
        if (Keys.b_ok.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << " saving new limit" << std::endl;
            Battery.Charger.set_time_limit(time_value);
            lcdCursorBlink (Display.get_fd(), false);
            break;
        }

        /* Defining Cancel action as aborting changes */
        if (Keys.b_cancel.short_time)
        {
            Keys.reset_all_buttons();
            std::cout << " changes not saved" << std::endl;
            lcdCursorBlink (Display.get_fd(), false);
            break;
        }
    }
}

bool float_compare(float a, float b)
{
    float EPSILON = 0.001;
    return fabs( a - b ) < EPSILON;
}