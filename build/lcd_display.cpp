#include "lcd_display.h"

Lcd_Manual::Lcd_Manual(int& _fd): fd(_fd) {}
Lcd::Lcd(int _rows, int _cols, int _bits, int _rs, int _strb, int _d0, int _d1, int _d2, int _d3, int _d4, int _d5, int _d6, int _d7)
    :rows(_rows), cols(_cols), bits(_bits), rs(_rs), strb(_strb), d0(_d0), d1(_d1), d2(_d2), d3(_d3), d4(_d4), d5(_d5), d6(_d6), d7(_d7)
{
    display_size = cols + 1;
    std::cout << "Initialising LCD display" << std::endl;
        std::cout << "LCD [rows][cols]: [" << rows << "][" << cols << "] using bits: " << bits << std::endl; 
        std::cout << "Initialisating at gpio: " << rs << " Lcd.RS" << std::endl;
        std::cout << "Initialisating at gpio: " << strb << " Lcd.STRB" << std::endl;
        std::cout << "Initialisating at gpio: " << d0 << " Lcd.D0" << std::endl;
        std::cout << "Initialisating at gpio: " << d1 << " Lcd.D1" << std::endl;
        std::cout << "Initialisating at gpio: " << d2 << " Lcd.D2" << std::endl;
        std::cout << "Initialisating at gpio: " << d3 << " Lcd.D3" << std::endl;
        std::cout << "Initialisating at gpio: " << d4 << " Lcd.D4" << std::endl;
        std::cout << "Initialisating at gpio: " << d5 << " Lcd.D5" << std::endl;
        std::cout << "Initialisating at gpio: " << d6 << " Lcd.D6" << std::endl;
        std::cout << "Initialisating at gpio: " << d7 << " Lcd.D7" << std::endl;
    fd = lcdInit(rows, cols, bits, rs, strb, d0, d1, d2, d3, d4, d5, d6, d7);
    if(fd != 0)
    {
        std::cout << "LCD uninitialised" << std::endl;
        //throw std::exception()
    }
    Manual_chrg = std::make_shared<Lcd_Manual>(fd);
}

const int Lcd::get_fd() const {return fd;}

/* ./   Welcome displays */
void Lcd::out_init()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Initialising    ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "                ") ;
}
void Lcd::out_welcome()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Smart Charger   ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "    Adam Smalira") ;
}
void Lcd::out_farewell()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Shuting down    ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "    bye bye     ") ;
}

/* Common displays for all modes */
/* ./main/manual/common/ */
/* ./main/automatic/common/ */
void Lcd::common_charge_volt(float set_voltage_limit, float max_charge_value)
{

    int chars_written = snprintf(buffer, display_size, "V: %1.3f        ", set_voltage_limit);
    if( chars_written < 0)
        abort();

    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Set stop volts: ");
   
    if(float_compare(set_voltage_limit, max_charge_value))
        {lcdPosition(fd, 0, 1); lcdPuts(fd, "-full charge    ");}
    else   
        {lcdPosition (fd, 0, 1) ; lcdPuts (fd, buffer);}
}
void Lcd::common_charge_time(int set_time_limit, int no_limit)
{
    int chars_written = snprintf(buffer, display_size, "minutes: %d        ", set_time_limit);
    if( chars_written < 0)
        abort();

    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Set time to stop");
    if(set_time_limit == no_limit)
        {lcdPosition(fd, 0, 1); lcdPuts(fd, "wait until full ");}
    else
        {lcdPosition(fd, 0, 1); lcdPuts(fd, buffer);}
}
void Lcd::common_charge_start(Batt& Battery, int cell_nr)
{
    float value = Battery.get_cell(cell_nr).get_voltage();
    int chars_written = snprintf(buffer, display_size, "%1d: %1.4fV       ", (cell_nr + 1), value);
    if( chars_written < 0)
        abort();
    //std::cout << "CHARS WRITTENNNNNNNNNNNNN: " << chars_written << std::endl;
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, " -- charging -- ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, buffer);
    
}

/* ./menu/  Main Menu displays */
void Lcd::main_automatic()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Choose mode:    ");
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "Automatic       "); 
}
void Lcd::main_manual()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Choose mode:    ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "Manual          ") ; 
}
void Lcd::main_test()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Choose mode:    ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "Test channels   ") ; 
}
void Lcd::main_not_yet()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "This option is  ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "not supported   ") ; 
}
void Lcd::main_testing()
{
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "                ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, "Testing channels") ;
}

/* ./menu/manual/  Manual Charging displays */
void Lcd_Manual::bat(Batt& Battery, int cell_nr)
{

    const int display_size = 16 + 1;
    char buffer[display_size];
    //char nr = static_cast<char>(cell_nr) + 49;
    volatile float value = Battery.get_cell(cell_nr).get_voltage();
    int chars_written = snprintf(buffer, display_size, "%1d: %1.3fV       ", (cell_nr + 1), value);
    if( chars_written < 0)
        abort();
    //#define snprintf_nowarn(...) (snprintf(__VA_ARGS__) < 0 ? abort() : (void)0)
    //snprintf_nowarn(buffer, display_size, "%1d: %1.3fV   ", (cell_nr + 1), (value)); //this define fixes snprintf bug "truncation warning in GCC"
    
    lcdPosition (fd, 0, 0) ; lcdPuts (fd, "Choose battery: ") ;
    lcdPosition (fd, 0, 1) ; lcdPuts (fd, buffer);
}



void thread_display_charging(Batt& Battery, Lcd& Display, int cell_NR)
{
    while(Battery.stop_charging_thread == false)
    {
        //std::cout << "Running thread DIPSPLAY" << std::endl;
        Display.common_charge_start(Battery, cell_NR);
        delay(500);
    }
    std::cout << "Stoping Dipslay" << std::endl;
}