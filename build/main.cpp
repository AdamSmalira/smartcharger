#include <iostream>
#include <thread>
#include <unistd.h>
#include <vector>
//#include <term.h> //sudo apt-get install libncurses5-dev
#include <wiringPi.h>
#include <cstdlib>

#include "batteries.h"
#include "functions.h"
#include "buttons.h"
#include "threads.h"
#include "lcd_display.h"

/* Defining GPIO pinout */
#define LCD_row 2
#define LCD_col 16
#define LCD_bit 8
#define LCD_rs 21
#define LCD_strb 20
#define LCD_d0 4
#define LCD_d1 10
#define LCD_d2 9
#define LCD_d3 11
#define LCD_d4 16
#define LCD_d5 12
#define LCD_d6 25
#define LCD_d7 24
#define CHRG_wait 17
#define CHRG_busy 27
#define KEY_Ok 22
#define KEY_Cancel 23
#define KEY_Left 5
#define KEY_Right 18
#define ADS_Pin 200
#define ADS_Adr 0x48

using namespace std;

int main()
{
    int gpio_fd;
    initialize_platform(gpio_fd);
    //Lcd Display{2, 16, 4, 21, 20, 16, 12, 25, 24, 0, 0, 0, 0}; //4bit version
    Lcd Display{LCD_row, LCD_col, LCD_bit, LCD_rs, LCD_strb, LCD_d0, LCD_d1, LCD_d2, LCD_d3, LCD_d4, LCD_d5, LCD_d6, LCD_d7}; //8bit version
    Display.out_init();
    delay(1000);
    Signals Charger{CHRG_wait, CHRG_busy};
    Keyboard Keys{KEY_Ok, "Ok", KEY_Cancel, "Cancel", KEY_Left, "Left", KEY_Right, "Right"};
    ADS1115 Voltmeter{ADS_Pin, ADS_Adr};

    Batt Battery{Charger};
    Battery.add_cell(26);
    Battery.add_cell(19);
    Battery.add_cell(13);
    Battery.add_cell(6);
    cout << "Charger status [waiting:busy]:" << Battery.Charger.get_waiting() << ": " << Battery.Charger.get_chrging() << endl;
    
    /* Reseting to default value and switching back to the output */
    Battery.change_to_input();
    Battery.change_to_output();

    Voltmeter.thread_read_write = thread(&thread_read_write_voltage, ref(Battery), ref(Voltmeter));
    try{
    while(true)
    {
        Display.out_welcome();
        cout << "./" << endl;
        Keys.if_buttons_have_been_pressed(nullptr);
        if (Keys.check_if_all_buttons_are_short())
        {
            Keys.reset_all_buttons();
            cout << "./menu/ Please select 'Automatic' or 'Manual' charging" << endl;
                short possition_main = 0;
                const short possition_main_min = 0;
                const short possition_main_max = 2;

                const short possition_automatic = 0;
                const short possition_manual = 1;
                const short possition_test = 2;
            while(true)
            {
                if(possition_main == possition_automatic)
                    Display.main_automatic();
                if(possition_main == possition_manual)
                    Display.main_manual();
                if(possition_main == possition_test)
                    Display.main_test();

                Keys.if_buttons_have_been_pressed(nullptr);

                /* Defining movement right and left buttons in menu */
                if(Keys.b_right.short_time && (possition_main < possition_main_max))
                {
                    Keys.reset_all_buttons();
                    cout << "./menu/  button right" << endl;
                    possition_main++;
                }
                if(Keys.b_left.short_time && (possition_main > possition_main_min))
                {
                    Keys.reset_all_buttons();
                    cout << "./menu/  button left" << endl;
                    possition_main--;
                }

                /* Defining actions for pressing OK for each possition */
                if(Keys.b_ok.short_time && (possition_main == possition_automatic) )
                {
                    Keys.reset_all_buttons();
                    cout << "./menu/automatic/ This option is not supported yet" << endl;
                    Display.main_not_yet();
                    delay(2000);
                }
                if(Keys.b_ok.short_time && (possition_main == possition_manual))
                {
                    Keys.reset_all_buttons();
                    cout << "./menu/  Manual Charging << endl" << endl;
                    manual_charging(Keys, Display, Battery);
                    cout << "./menu/" << endl;
                }
                if(Keys.b_ok.short_time && (possition_main == possition_test))
                {
                    Keys.reset_all_buttons();
                    cout << "./menu/ testing channels" << endl;
                    Display.main_testing();
                    Battery.channel_test();
                    delay(500);
                    cout << "./menu/" << endl;
                }

                /* Defining cancel*/
                if(Keys.b_cancel.short_time)
                {
                    Keys.reset_all_buttons();
                    cout << "./menu/ cancel" << endl;
                    break;
                }
                Keys.reset_all_buttons();
                delay(100);
            }
        }
        delay(200);
    }
    }catch(const exception& er)
    {
        cout << er.what() << endl;
        Battery.stop_charging_thread = true;
        Battery.Charger.thread_countdown_stop = true;
        Voltmeter.thread_read_write_stop = true;
        if(Battery.Charger.thread_countdown.joinable())
            Battery.Charger.thread_countdown.join();
        if(Voltmeter.thread_read_write.joinable())
            Voltmeter.thread_read_write.join();
        if(Display.thread_lcd.joinable())
            Display.thread_lcd.join();
        Battery.stop_chrg_all();
        Battery.change_to_input();
        Display.out_farewell();
        return EXIT_FAILURE;
    }
    Voltmeter.thread_read_write.join();


    return EXIT_SUCCESS;
}
