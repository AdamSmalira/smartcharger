#include "buttons.h"

/* Buttons class */
Buttons::Buttons(int gpio, std::string desc): gpio_nr(gpio), description(desc) 
{ std::cout << "Initialisating at gpio: " << gpio_nr << " button " << description << std::endl; }

int Buttons::get_gpio() { return gpio_nr;}


/* Keyboard class */
Keyboard::Keyboard(int gpio_ok, std::string desc_ok, int gpio_cancel, std::string desc_cancel, int gpio_left, std::string desc_left, int gpio_right, std::string desc_right)
    :b_ok(gpio_ok, desc_ok), b_cancel(gpio_cancel, desc_cancel), b_left(gpio_left, desc_left), b_right(gpio_right, desc_right) {}

short Keyboard::check_button_status(Buttons& button)
{
    /* counting time for long or short signal */
    int counter = 0;
    while (digitalRead(button.get_gpio()))
    {
        delay(100);
        counter++;
        std::cout << counter << std::endl;
        if(counter > 9)
        {
            break;
        }
    }
    if (counter > 4)
    {
        button.short_time = false;
        button.long_time = true;
        return 1;
    }
    else
    {
        button.short_time = true;
        button.long_time = false;
        return 1;
    }
}
void Keyboard::if_buttons_have_been_pressed(bool* thread_kill)
{
    short a = 0, b = 0, c = 0, d = 0;
    while(true)
    {
        if(thread_kill != nullptr) // breaking scanning buttons
            if(*thread_kill == true)
                return;

        if (digitalRead(b_cancel.get_gpio()))
            a = check_button_status(b_cancel);
            
        if (digitalRead(b_ok.get_gpio()))
            b = check_button_status(b_ok);

        if (digitalRead(b_left.get_gpio()))
            c = check_button_status(b_left);

        if (digitalRead(b_right.get_gpio()))
            d = check_button_status(b_right);
        
        if(a + b + c + d)
        {
            return;
        }
        else
            delay(100);
    }
}

bool Keyboard::check_if_all_buttons_are_short()
{
    return (b_cancel.short_time + b_ok.short_time + b_left.short_time + b_right.short_time);
}

void Keyboard::reset_all_buttons(){
    b_ok.short_time = false;
    b_ok.long_time = false;

    b_cancel.short_time = false;
    b_cancel.long_time = false;

    b_left.short_time = false;
    b_left.long_time = false;

    b_right.short_time = false;
    b_right.long_time = false;
}
