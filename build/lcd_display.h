#ifndef _LCD_DISPLAY_H_
#define _LCD_DISPLAY_H_

#include <iostream>
#include <memory>
#include <cstring>
#include <cstdio>
#include <thread>

class Lcd;

#include "lcd.h"
#include "batteries.h"
#include "functions.h"



    
class Lcd_Manual;

class Lcd{
private:
    const int rows;
    const int cols;
    const int bits;
    const int rs;
    const int strb;
    const int d0;
    const int d1;
    const int d2;
    const int d3;
    const int d4;
    const int d5;
    const int d6;
    const int d7;
    int fd = -1;
    char buffer[17];
    int display_size;
public:
    Lcd(int _rows, int _cols, int _bits, int _rs, int _strb, int _d0, int _d1, int _d2, int _d3, int _d4, int _d5, int _d6, int _d7);
    const int get_fd() const;
    std::thread thread_lcd;

    void out_init();
    void out_farewell();
    void out_welcome();


    void main_automatic();
    void main_manual();
    void main_not_yet();
    void main_test();
    void main_testing();
    std::shared_ptr<Lcd_Manual> Manual_chrg;

    /* /main/manual/common/ */ 
    /* /main/automatic/common/ */
    void common_charge_volt(float set_voltage_limit, float border);
    void common_charge_time(int set_time_limit, int pernament);
    void common_charge_start(Batt& Battery, int cell_nr);
    //class Automatic;
    
};

class Lcd_Manual
{
private:
    int &fd;
public:
    Lcd_Manual(int& _fd);
    void bat(Batt& Battery, int cell_nr);
};

void thread_display_charging(Batt& Battery, Lcd& Display, int cell_NR);

#endif //_LCD_DISPLAY_H_
