#include "threads.h"

ADS1115::ADS1115(int pinbase, int _address): address(_address)
{
    for(size_t i = 0; i < pin.size(); i++)
    {
        pin[i] = pinbase + i;
        std::cout << "Initialisating at virtual gpio: " << pin[i] << " adc channel[" << i << "]" << std::endl; 
    }
    ads1115Setup(pin[0], address);
    set_gain(0);
    std::cout << "Initialisating adc1115 konwerter at: " << address << std::endl;
}
void ADS1115::set_gain(int _gain) { 
    gain = _gain;
    switch(gain)
    {
        case 0:
            resolution = 6.144f;
            break;
        case 1:
            resolution = 4.096f;
            break;
        default:
            resolution = -1;
            std::cout << "Wrong gain is set, use propper value" << std::endl;
    }
    digitalWrite(pin[0], gain);
}
int ADS1115::get_gain() {return gain;}
int ADS1115::get_value(int channel) {return analogRead(pin[channel]);}
float ADS1115::get_volts(int value)
{ 
    float volts = static_cast<float>(value) * (resolution/conversion15bit);
    return volts;
}
void thread_read_write_voltage(Batt& Battery, ADS1115& Voltmeter)
{

    while (true)
    {
        for (int i = 0; i < 4; i++)
        {
            if(Voltmeter.thread_read_write_stop)
            {
                std::cout << "Stoping reading voltages" << std::endl;
                Voltmeter.thread_read_write_stop = false;
                return;
            }
            Battery.get_cell(i).set_voltage(Voltmeter.get_volts(Voltmeter.get_value(i)));
            std::cout << "[" << i << "]: " << Voltmeter.get_value(i) << " " << Battery.get_cell(i).get_voltage() << " ";
        }
        std::cout << std::endl;
        delay(500);
    }
}



